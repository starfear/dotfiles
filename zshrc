# Created by newuser for 5.6.2
powerline-daemon -q
. /usr/lib/python3.7/site-packages/powerline/bindings/zsh/powerline.zsh

export TERM="xterm-256color"

# Powerlevel9K theme
source  ~/powerlevel9k/powerlevel9k.zsh-theme

# Auto-Suggestions
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# SAVEHIST=9999999
# HISTFILE=~/.zsh_history
